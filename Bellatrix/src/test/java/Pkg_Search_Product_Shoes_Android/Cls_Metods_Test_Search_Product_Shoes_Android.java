package Pkg_Search_Product_Shoes_Android;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import Pkg_Lib_methods.Cls_Lib_Methods;
import Plataforma.testBase;
import Plataforma.Android.Utils.*;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;


public class Cls_Metods_Test_Search_Product_Shoes_Android extends testBase {
	
	AndroidDriver<AndroidElement> driver;
	int tryElement = 3;
	Cls_Lib_Methods Methods = new Cls_Lib_Methods();
	
	public void Search_Product_Shoes_Android(AndroidDriver<AndroidElement> driver) throws Exception {
	
		this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Open ebay Home, anbd search field find Article", driver);
		Methods.WaitingForMobileElement(search, tryElement);
		search.click();
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Select the field find Article", driver);
		Methods.WaitingForMobileElement(searchArticle, tryElement);
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Display all options for Shoe's", driver);
		searchArticle.setValue("shoes");
		Methods.WaitingForMobileElement(opcSelect_Articule, tryElement);
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Select the option Men's Shoes", driver);
		opcSelect_Articule.click();
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("The system shows the Men's Shoes", driver);
		Thread.sleep(3000);
		Methods.ScrollLeftTouchSize();
		Methods.ScrollLeftTouchSize();
		Methods.ScrollLeftTouchSize();
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Select the size 10", driver);		
		Methods.WaitingForMobileElement(opcSize_Ten, tryElement);
		opcSize_Ten.click();
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Select filters", driver);		
		Methods.WaitingForMobileElement(opcSelect_filter, tryElement);
		opcSelect_filter.click();
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Select Brand", driver);		
		Methods.WaitingForMobileElement(opcSelect_Brand, tryElement);
		opcSelect_Brand.click();
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Select Puma Brand", driver);		
		Methods.WaitingForMobileElement(opcSelect_PumaBrand, tryElement);
		opcSelect_PumaBrand.click();
		Methods.WaitingForMobileElement(opcSelect_Done, tryElement);
		opcSelect_Done.click();
		String balance = countArticle.getText();
		System.out.println("Android: Total Count is: " + balance );
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Get the Price", driver);		
		Methods.WaitingForMobileElement(SortArticle, tryElement);
		SortArticle.click();
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Sort tehe articles in Descendent form ", driver);		
		Methods.WaitingForMobileElement(opcPrice_Desc, tryElement);
		opcPrice_Desc.click();
		ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Select The Price", driver);

	}
	
}
