package Pkg_Search_Product_Shoes_Android;

import org.testng.annotations.Test;

import Pkg_Lib_methods.Cls_Lib_Methods;
import Plataforma.testBase;
import Plataforma.Android.Utils.ExcelDataProviderAndroid;
import Plataforma.Android.Utils.ExtentTestManagerAndroid;
import io.appium.java_client.android.AndroidDriver;
import Plataforma.Dom_Elements_Android;


import java.lang.reflect.Method;

public class Cls_Search_Product_Shoes_Android extends testBase {
	
	Cls_Metods_Test_Search_Product_Shoes_Android objMetodCobroSinR = new Cls_Metods_Test_Search_Product_Shoes_Android() ;	
	public AndroidDriver getDriver() {
		return driver;
	}

	@Test(dataProvider = "dataTest", dataProviderClass = ExcelDataProviderAndroid.class)
	public void Search_Product_Shoes_Android(String pais, String usuario, String clave, String NumeroDeTC, String ValorEnvio,
		String NumCelularEnvio,String NumeroCelConRegistro,String Pin,	String NumeroDoc, String tipoBusqueda,
		String ejecutar,String Full_TC,String NumeroExtranjero,String IndicativoExt,String FechaMesAno,String Cvc,
		String NombreTc,String ApellidoTc,String NumeroNoRegistrado,Method test) throws Exception {  

		ExtentTestManagerAndroid.startTest(test.getName()  + " " + pais,
				"Start Test Case from Country: " + pais);

		objMetodCobroSinR.Search_Product_Shoes_Android(driver);;

		String reporte = ExtentTestManagerAndroid.getTest().getDescription();
		ExtentTestManagerAndroid.getTest().setDescription(reporte + " TEST: " + test.getName());


	}


}
