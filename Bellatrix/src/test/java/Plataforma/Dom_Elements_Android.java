package Plataforma;

import java.util.List;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class Dom_Elements_Android extends InvokeTestNGJava {
	
	@AndroidFindBy(id  = "com.android.chrome:id/url_bar")
	public MobileElement Imput_SafariUrl;
	@AndroidFindBy(id = "com.ebay.mobile:id/search_box")
	public MobileElement search;
	@AndroidFindBy(id = "com.ebay.mobile:id/search_src_text")
	public MobileElement searchArticle;
	@AndroidFindBy(id = "com.ebay.mobile:id/button_filter")
	public MobileElement opcSelect_filter;
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView[1]")
	public MobileElement opcSelect_Brand;
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.CheckedTextView[6]")	
	public MobileElement opcSelect_PumaBrand;
	@AndroidFindBy(id = "com.ebay.mobile:id/button_done")
	public MobileElement opcSelect_Done;
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout[2]/android.widget.ListView/android.widget.RelativeLayout[3]/android.widget.TextView")
	public MobileElement opcSelect_Articule;
	@AndroidFindBy(id = "com.ebay.mobile:id/recyclerview_items")
	public MobileElement searchShoeSize;
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[2]")
	public MobileElement opcSize_Ten;
	@AndroidFindBy(id = "com.ebay.mobile:id/textview_item_count")
	public MobileElement countArticle;
	@AndroidFindBy(id = "com.ebay.mobile:id/button_sort")
	public MobileElement SortArticle;
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.CheckedTextView")
	public MobileElement opcPrice_Desc;
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.CheckedTextView")
	public MobileElement opcPrice_Asc;
	@AndroidFindBy(id = "com.ebay.mobile:id/textview_item_price")
	public List<MobileElement> priceArticle;

}
