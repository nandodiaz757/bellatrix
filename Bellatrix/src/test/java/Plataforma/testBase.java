package Plataforma;

import io.appium.java_client.android.AndroidDriver;
import org.apache.tools.ant.util.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import Pkg_Lib_methods.Cls_Lib_Methods;
import Pkg_Search_Product_Shoes_Android.Cls_Metods_Test_Search_Product_Shoes_Android;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import Plataforma.Dom_Elements_Android;
import Plataforma.Android.Utils.Email;
import Plataforma.Android.Utils.ExtentTestManagerAndroid;
import Plataforma.Android.Utils.RecordingScreen;
import Plataforma.Android.Utils.Slack;

public class testBase extends Dom_Elements_Android {
	
	public static DesiredCapabilities capabilities;
	public static String language;
	public static String	udid;
	public static AndroidDriver driver;
	public static boolean permisosGenerarEvidencia = true;
	public static boolean permisosEnviarNotificacionesCorreo = true;
	public static boolean permisosEjecucionMac = false;
	public static boolean permisosIntegracionXlsx = false;
	public static Cls_Metods_Test_Search_Product_Shoes_Android cls_Metods_Test_Search_Product_Shoes_Android;
	public static Cls_Lib_Methods cls_Lib_Methods;
	public static Dom_Elements_Android dom_Elements_Android ;
	public static int count = -1;
	public static String metodo = "";
	public static List<String> imprimirExcel = new ArrayList<String>();
	public static List<String> ubicacionResultados = new ArrayList<String>();
	public static String fecha = "";
	public static int datosExcelCreados = 0;
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest reporte;
	public static List<String> steps = new ArrayList<String>();
	public static int time = 120;
	public static int port = 0;
	public static boolean permisoGenerarVideos = false;
	public static boolean permisoEnviarCorreo = true;
	public static boolean permisoEnviarSlack = true;
	public static String projectPath = System.getProperty("user.dir");
	public static Properties prop = new Properties();
	@SuppressWarnings("rawtypes")


	@BeforeSuite()
	public void suite() throws Exception {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String requiredDate = df.format(new Date()).toString();
		fecha = requiredDate.replace(":", "-").replace(" ", "_");

	}
	
	public AndroidDriver getDriver() {
		return driver;
	}
	

	@AfterSuite()
	public void finish(ITestContext context) throws Exception{
	
		try {
			
			Email.sendEmail(
					"Resultados De La Suite De Pruebas En La Regresion "
							+ context.getCurrentXmlTest().getParameter("pais").toString(),
					"Fecha De Ejecucion Suite " + fecha + " " + context.getName().toString(),
					projectPath + "/Android/Android_Results/Reportes/ExtentReportResultsAndroid_" + fecha + ".html",
					"ExtentReportResultsAndroid_" + fecha + ".html");
		} catch (Exception ex) {

			System.out.println(
					"REGRESION: ERROR AL ENVIAR NOTIFICACION POR CORREO O SLACK AL TERMINAR LA SUITE DE PRUEBAS "
							+ ex.toString());

		}

	}

	@AfterMethod()
	public void finishMethod(Method method, ITestResult iTestResult, ITestContext context) throws Exception {

	    String resultado;

		if (iTestResult.getStatus() != 1) {

			RecordingScreen.stopFail(method.getName());
			String pais = context.getCurrentXmlTest().getParameter("pais").toString();
			Email.sendEmail("Resultados Del Test " + method.getName() + " Regresion " + pais,
					"Fecha De Ejecucion " + fecha + " Error: " + iTestResult.getThrowable().toString(), "", "");

			if (permisoEnviarSlack == true) {
				File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				Slack.uploadFile(screenShot, "Resultados Del Test " + method.getName() + " Regresion " + pais + " "
						+ "Fecha De Ejecucion " + fecha + " Error: " + iTestResult.getThrowable().toString());
				FileUtils.delete(screenShot);


			}
		} else {
			ExtentTestManagerAndroid.endTest();
		}

		driver.closeApp();
	}

	@BeforeMethod()
	public void AbrirApp(Method method, ITestContext context) throws Exception {
				
		InputStream entrada = new FileInputStream("Config.properties");
		prop.load(entrada);
		udid = prop.getProperty("udid");
		String	deviceName = prop.getProperty("deviceName");
		String	platformVersion = prop.getProperty("platformVersion");
		String	platformName = prop.getProperty("platformName");
		String	appPackage = prop.getProperty("appPackage");
		String	appActivity = prop.getProperty("appActivity");
		String	UrlAppium = prop.getProperty("UrlAppium");
			
		
		capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName",deviceName);
		capabilities.setCapability("autoGrantPermissions", true);
		capabilities.setCapability("android.permission.RECORD_AUDIO", true);
		capabilities.setCapability("android.permission.INTERNET", true);
		capabilities.setCapability("android.permission.CAMERA", true);
		capabilities.setCapability("android.permission.READ_EXTERNAL_STORAGE", true);
		capabilities.setCapability("android.permission.WRITE_EXTERNAL_STORAGE", true);
		capabilities.setCapability("appPackage", appPackage);
		capabilities.setCapability("appActivity", appActivity);
		capabilities.setCapability("fastReset", false);
		capabilities.setCapability("noReset", true);
		capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("automationName", "UiAutomator2");
		capabilities.setCapability("newCommandTimeout", 72000);
		capabilities.setCapability("deviceReadyTimeout", 100);
		capabilities.setCapability("androidDeviceReadyTimeout", 300);
		capabilities.setCapability("autoWebviewTimeout", 5);
		capabilities.setCapability("adbExecTimeout", 30000);
		capabilities.setCapability("avdLaunchTimeout", 70);
		capabilities.setCapability("avdReadyTimeout", 100);
		capabilities.setCapability("androidInstallPath", "./Android/Android_Results/Videos/");

		language = "es";

		try {

			if (method.getName().equals("testViewMoreInformationIngles")) {

				language = "en";
			}

			if (method.getName().equals("testViewMoreInformationPortugues")) {

				language = "pt";

			}
		} catch (Exception ex) {

		}

		capabilities.setCapability("language", language);
		capabilities.setCapability("locale", "CO");

		driver = new AndroidDriver(new URL(UrlAppium), capabilities);
		
	}

}
