package Plataforma.Android.Utils;

import java.io.File;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.tools.ant.util.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import Plataforma.testBase;

public class Email extends testBase {

	public static void sendEmail(String subject, String mensaje, String adjuntos, String nombre) throws Exception {

		if (permisoEnviarCorreo == true) {

			final String Username = "fernando.diaz@xxxx.com";
			final String PassWord = "xxx019";
			String To = "fernando.diaz@xxx.com";
			String Subject = subject;

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(Username, PassWord);
				}
			});

			MimeMultipart multiParte = new MimeMultipart();
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(To));

			BodyPart texto = new MimeBodyPart();
			texto.setText(mensaje);
			multiParte.addBodyPart(texto);

			File base64Screenshot = null;

			if (!adjuntos.equals("")) {

				BodyPart adjunto = new MimeBodyPart();
				adjunto.setDataHandler(new DataHandler(new FileDataSource(adjuntos)));
				adjunto.setFileName(nombre);
				multiParte.addBodyPart(adjunto);

			} else {

				base64Screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				BodyPart imagen = new MimeBodyPart();
				String contentId = "imagen1";
				String htmlText = "<img align=\" center \" src=\"" + contentId + " \"><br>";
				imagen.setContent(htmlText + "<br>", "text/html");
				multiParte.addBodyPart(imagen);
				imagen = new MimeBodyPart();
				DataSource fds = new FileDataSource(base64Screenshot);
				imagen.setDataHandler(new DataHandler(fds));
				imagen.setHeader("Content-ID", "<" + contentId + ">");
				imagen.setFileName("evidencia");
				multiParte.addBodyPart(imagen);

			}

			message.setSubject(Subject);
			message.setText(mensaje);
			message.setContent(multiParte);

			Transport.send(message);

			if (adjuntos.equals("")) {

				FileUtils.delete(base64Screenshot);
			}

		}

	}

}
