package Plataforma.Android.Utils;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import Plataforma.testBase;
import io.appium.java_client.MobileElement;

public class Wait extends testBase {

	public static  FluentWait waiting = new WebDriverWait(driver, time).pollingEvery(500, TimeUnit.MILLISECONDS)
			.withTimeout(time, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

	public static void cargando() throws Exception {

		allText();

		List<MobileElement> icono = driver.findElements(By.id("com.grability.rappi:id/lottieAnimationView"));

		if (icono.size() > 0) {

			System.out.println("WAIT: CARGO CON EXITO (lottieAnimationView)");

			try {
				waiting.until(ExpectedConditions
						.invisibilityOf(driver.findElement(By.id("com.grability.rappi:id/lottieAnimationView"))));
			} catch (Exception ex) {
			}

		}

	}

	public static void element(MobileElement id) throws Exception {

		waiting.until(ExpectedConditions.visibilityOf(id));

		waiting.until(ExpectedConditions.elementToBeClickable(id));

	}

	public static void allText() throws Exception {

		waiting.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.TextView")));
		// waiting.(By.className("android.widget.TextView"));

		// wait.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.TextView")));

		// System.out.println("WAIT: SE CARGARON TODOS LOS TEXTOS ");
	}

	public static void allViewGroup() throws Exception {

		waiting.until(ExpectedConditions.presenceOfElementLocated(By.className("android.view.ViewGroup")));

		System.out.println("WAIT: SE CARGARON TODOS LOS VIEWGROUP ");
	}

	public static void allCountry() throws Exception {

		waiting.until(ExpectedConditions.presenceOfElementLocated(By.className("android.view.ViewGroup")));

		// System.out.println("WAIT: SE CARGARON TODOS LOS PAISES ");
	}

	public static void allImages() throws Exception {

		waiting.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.ImageView")));

		// System.out.println("WAIT: SE CARGARON TODAS LAS IMAGENES ");
	}

	public static void allPasillos() throws Exception {

		waiting.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")));

		List<MobileElement> itemsPasillos = driver.findElements(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]"));

		int detener = 0;

		while (itemsPasillos.size() == 0) {

			Thread.sleep(1000);

			itemsPasillos = driver.findElements(By.xpath(
					"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]"));

			detener++;

			if (detener == 10 && itemsPasillos.size() == 0) {

				System.out.println("WAIT: NO SE CARGARON TODOS LOS ELEMENTOS DE PASILLOS ");

				throw new SkipException("WAIT: NO SE CARGARON TODOS LOS ELEMENTOS DE PASILLOS  ");

			}

		}

		System.out.println("WAIT: SE CARGARON TODOS LOS ELEMENTOS QUE CONFORMAN EL MODULO DE PASILLOS ");
	}

	public static void allPayment() throws Exception {

		waiting.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.id("com.grability.rappi:id/textView_label_payment_method")));

		System.out.println("WAIT: SE CARGARON TODOS LOS ELEMENTOS QUE CONFORMAN EL CHECKOUT ");

	}

	public static void allCountries() throws Exception {

		waiting.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.id("com.grability.rappi:id/textView_country_name")));

		waiting.until(ExpectedConditions.elementToBeClickable(By.id("com.grability.rappi:id/textView_country_name")));

		System.out.println("WAIT: SE CARGARON TODOS LOS ELEMENTOS QUE CONFORMAN LA LISTA DE PAISES ");

	}
}
