package Plataforma.Android.Utils;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.AndroidDriver;

public class ExtentTestManagerAndroid {

//	AndroidDriver driver;
	static Map extentTestMap = new HashMap();
	static ExtentReports extent = ExtentManagerAndroid.getReporter();

	public static synchronized ExtentTest getTest() {
		return (ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId()));
	}

	public static synchronized void endTest() {
		extent.endTest((ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId())));
	}

	public static synchronized ExtentTest startTest(String testName, String desc) {
		ExtentTest test = extent.startTest(testName, desc);
		extentTestMap.put((int) (long) (Thread.currentThread().getId()), test);
		return test;
	}
	
	public static void GetEvidenceExtendTestAndroid(String MensajeAsociado,AndroidDriver Dirve) {
	String base64Screenshot = "data:image/png;base64,"
			+ ((TakesScreenshot) Dirve).getScreenshotAs(OutputType.BASE64);
	ExtentTestManagerAndroid.getTest().log(LogStatus.INFO,MensajeAsociado+ ": ",
			ExtentTestManagerAndroid.getTest().addBase64ScreenShot(base64Screenshot));
	
	}
}
