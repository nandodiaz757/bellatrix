package Plataforma.Android.Utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Slack {

	public static void uploadFile(File archivo, String mensaje) throws Exception {

		ProcessBuilder processBuilder = new ProcessBuilder();

		processBuilder.command("curl", "-F", " file=@" + archivo + "", "-F", "initial_comment=" + mensaje + "", "-F",
				"channels=CL8EY5Z1C", "-H",
				"Authorization: Bearer xoxp-674948801266-674948801538-686256852612-f20d2f75c9104ebfbaa7c8f8ee283ca4",
				"https://slack.com/api/files.upload");

		Process process = processBuilder.start();

		StringBuilder output = new StringBuilder();

		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

		String line;
		while ((line = reader.readLine()) != null) {
			output.append(line + "\n");
		}

		int exitVal = process.waitFor();
		if (exitVal == 0) {
			System.out.println("Success!");
//			System.out.println(output);
//			System.exit(0);
		} else {
			// abnormal...
		}

	}

	public static void notificacion(String mensaje) throws Exception {

		String urly = "https://hooks.slack.com/services/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";
		URL obj = new URL(urly);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(
				"{ \"text\": \"<@UKUTWPKFU> approved your travel request. Book any airline you like by continuing below.\","
						+ " \"attachments\": [\n" + "        {\n" + "            \"text\": \"Choose a game to play\",\n"
						+ "            \"fallback\": \"You are unable to choose a game\",\n"
						+ "            \"callback_id\": \"wopr_game\",\n" + "            \"color\": \"#3AA3E3\",\n"
						+ "            \"attachment_type\": \"default\",\n" + "            \"actions\": [\n"
						+ "                {\n" + "                    \"name\": \"game\",\n"
						+ "                    \"text\": \"Chess\",\n" + "                    \"type\": \"button\",\n"
						+ "                    \"image_url\": \"" + mensaje + "\"\n" + "                },\n"
						+ "                {\n" + "                    \"name\": \"game\",\n"
						+ "                    \"text\": \"Falken's Maze\",\n"
						+ "                    \"type\": \"button\",\n" + "                    \"file\": \"" + mensaje
						+ "\"\n" + "                },\n" + "                {\n"
						+ "                    \"name\": \"game\",\n"
						+ "                    \"text\": \"Thermonuclear War\",\n"
						+ "                    \"style\": \"danger\",\n" + "                    \"type\": \"button\",\n"
						+ "                    \"url\": \"" + mensaje + "\",\n" + "                    \"confirm\": {\n"
						+ "                        \"title\": \"Are you sure?\",\n"
						+ "                        \"text\": \"Wouldn't you prefer a good game of chess?\",\n"
						+ "                        \"ok_text\": \"Yes\",\n"
						+ "                        \"dismiss_text\": \"No\"\n" + "                    }\n"
						+ "                }\n" + "            ]\n" + "        }\n" + "    ]}");
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("Response Code : " + responseCode);

		BufferedReader iny = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String output;
		StringBuffer response = new StringBuffer();

		while ((output = iny.readLine()) != null) {
			response.append(output);
		}
		iny.close();

		// printing result from response
		System.out.println(response.toString());
	}

}
