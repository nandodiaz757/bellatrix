package Plataforma.Android.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import Plataforma.testBase;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class ScrollAndroid extends testBase {

	public static void scrollCanasta() throws InterruptedException {

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getHeight() * 0.5);

		int scrollHeightEnd = (int) (dimension.getHeight() * 0.3);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(0, scrollHeightStart)).waitAction().moveTo(PointOption.point(0, scrollHeightEnd))
				.release().perform();

		System.out.println("SCROLL_ANDROID: se hace scrollCanasta");

		Thread.sleep(2000);

	}

	public static void scrollDown() throws InterruptedException {

		Thread.sleep(1000);

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getHeight() * 0.5);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(0, scrollHeightStart - 100)).waitAction().moveTo(PointOption.point(0, 10))
				.release().perform();

		System.out.println("SCROLL_ANDROID: se hace scroll down");

		Thread.sleep(1000);

	}

	public static void valueOnBasket() throws InterruptedException {

		Thread.sleep(1000);

		TouchAction touchAction = new TouchAction(driver);
		touchAction
				.press(PointOption.point(800,
						(int) (driver.findElement(By.id("com.grability.rappi:id/textView_name")).getLocation().getY()
								* 0.7)))
				.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
				.moveTo(PointOption.point(800,
						(int) (driver.findElement(By.id("com.grability.rappi:id/textView_name")).getLocation().getY()
								* 0.2)))
				.release().perform();

		System.out.println("SCROLL_ANDROID: valueOnBasket");

		Thread.sleep(1000);

	}

	public static void scrollQuitBrandroom() {

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getWidth() * 0.9);

		int scrollHeightEnd = (int) (dimension.getWidth() * 0.1);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(scrollHeightStart, driver.manage().window().getSize().getHeight() / 2))
				.waitAction()
				.moveTo(PointOption.point(scrollHeightEnd, driver.manage().window().getSize().getHeight() / 2))
				.release().perform();

		System.out.println("SCROLL_ANDROID: QUITAR BRANDROOMS");

	}

	public static void scrollSearchPatrocinado() {

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getHeight() * 0.6);

		int scrollHeightEnd = (int) (dimension.getHeight() * 0.1);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(0, scrollHeightStart)).waitAction().moveTo(PointOption.point(0, scrollHeightEnd))
				.release().perform();

		System.out.println("SCROLL_ANDROID: BUSCANDO PRODUCTOS");

	}

	public static void scrollRight() {

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getHeight() * 0.5);

		int scrollHeightEnd = (int) (dimension.getHeight() * 0.1);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(scrollHeightStart, 0)).waitAction().moveTo(PointOption.point(scrollHeightEnd, 0))
				.release().perform();

		System.out.println("SCROLL_ANDROID: SCROLL RIGHT ...");

	}

	public static void scrollLeft() {

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getWidth() * 0.5);

		int scrollHeightEnd = (int) (dimension.getWidth() * 0.1);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(scrollHeightEnd, 0)).waitAction().moveTo(PointOption.point(scrollHeightStart, 0))
				.release().perform();

		System.out.println("SCROLL_ANDROID: SCROLL LEFT ...");

	}

	public static void scrollRightSubCorridors() {

		System.out.println("ANDROID_STORE_PAGE: Swiping Rigth on the SubCorridors");

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getWidth() * 0.6);

		int scrollHeightEnd = (int) (dimension.getWidth() * 0.2);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(scrollHeightStart, 0)).waitAction().moveTo(PointOption.point(scrollHeightEnd, 0))
				.release().perform();

		System.out.println("SCROLL_ANDROID: se hace scroll right");

	}

	// convert image locator of type File to base64 encoded image
	public  By imageLocator(File f) throws Exception {
		System.out.println("UTILS: Encoding the following image file to base64: " + f.getAbsolutePath());
		String base64image = Base64.getEncoder().encodeToString(Files.readAllBytes(f.toPath()));
		return MobileBy.image(base64image);
	}

	// Image path constructor
	public  String imageLocatorPath(String imageResource) {
		return ("imageLocators/" + imageResource);
	}

	public static void switchToWebContext() throws Exception {

		Thread.sleep(2000);
		List<String> contextWeb = new ArrayList<String>();
		Set<String> contextHandles = driver.getContextHandles();

		for (String contextName : contextHandles) {

			System.out.println("CONTEXT ENCONTRADO " + contextName);

			if (contextName.contains("grability")) {

				contextWeb.add(contextName);

			}
		}

		if (contextWeb.size() == 0) {

			System.out.println("CONTEXT ENCONTRADOS " + contextWeb.size());

		}

		if (contextWeb.size() < 2 && contextWeb.size() > 0) {
			
			driver.context(contextWeb.get(0));
			System.out.println("CONTEXT SELECCIONADO " + contextWeb.get(0));

		} else {

			int count = 0;

			System.out.println("CONTEXT ELSE " + contextWeb.size());

			while (count < contextWeb.size()) {

				System.out.println("CONTEXT ENCONTRADO WHILE " + contextWeb.get(count));
				if (contextWeb.get(count).contains("grability")) {

					driver.context(contextWeb.get(count));

					count = contextWeb.size();

				} else {

					count++;

				}

			}

		}

		Thread.sleep(2000);

	}

	public static void switchToNativeContext() throws Exception {

		Thread.sleep(2000);
		Set<String> contextHandles = driver.getContextHandles();

		String contextNative = "";
		for (String s : contextHandles) {
			System.out.println("Context : " + s);
			for (String contextName : contextHandles) {
				if (contextName.contains("NATIVE")) {

					contextNative = contextName;

				}
			}
		}

		driver.context(contextNative);

		Thread.sleep(2000);
	}

	public static void Scroll(MobileElement desde, MobileElement hasta) throws Exception {

		try {

			TouchAction touchAction = new TouchAction(driver);
			touchAction.longPress(PointOption.point(desde.getLocation().getX(), desde.getLocation().getY()))
					.moveTo(PointOption.point(hasta.getLocation().getX(), hasta.getLocation().getY()))

					.release().perform();

			System.out.println("SCROLL_ANDROID: SCROLL DESDE X ELEMENTO HASTA X ELEMENTO ");

		} catch (Exception e) {
			System.out.println("ERROR HACIENDO SCROLL X ELEMENTO HASTA X ELEMENTO " + e);
		}

	}

	public static void scrollPress(MobileElement desde, MobileElement hasta) throws Exception {

		try {

			TouchAction touchAction = new TouchAction(driver);
			touchAction.press(PointOption.point(desde.getLocation().getX(), desde.getLocation().getY()))
					.moveTo(PointOption.point(hasta.getLocation().getX(), hasta.getLocation().getY()))

					.release().perform();

			System.out.println("SCROLL_ANDROID: SCROLL DESDE X ELEMENTO HASTA X ELEMENTO ");

		} catch (Exception e) {
			System.out.println("ERROR HACIENDO SCROLL X ELEMENTO HASTA X ELEMENTO " + e);
		}

	}

	public static void ScrollAlinear() throws Exception {
		try {

			TouchAction touchAction = new TouchAction(driver);
			touchAction.press(PointOption.point(800, driver.manage().window().getSize().getHeight() - 500))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1))).moveTo(PointOption.point(800, (300)))
					.release().perform();
		} catch (Exception e) {
			System.out.println("Error Alinear Scroll" + e);
		}

	}

	public static void scrollProductosComplementariosRelacionados() throws Exception {

		Thread.sleep(1000);

		MobileElement elemento = (MobileElement) driver.findElement(By.id("com.grability.rappi:id/scrollView_content"));

		Dimension dimension = elemento.getSize();

		int scrollHeightStart = (int) (dimension.getHeight() * 0.6);

		int scrollHeightEnd = (int) (dimension.getHeight() * 0.2);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(0, scrollHeightStart)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
				.moveTo(PointOption.point(0, scrollHeightEnd)).release().perform();

		Thread.sleep(1000);

	}

	public static void scrollClickOnCanasta(int getY) throws Exception {

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getHeight() * 0.5);

		int scrollHeightEnd = (int) (dimension.getHeight() * 0.3);

		System.out.println("SCROLL_ANDROID: se hace scrollCanasta");

		if (getY > driver.manage().window().getSize().getHeight() / 2) {

			TouchAction action = new TouchAction(driver);
			action.press(PointOption.point(0, scrollHeightStart))
					.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
					.moveTo(PointOption.point(0, scrollHeightEnd)).release().perform();

			System.out.println("ANDROID_PRODUCT_DETAILS: SCROLL UBICANDO EL ELEMENTO SOBRE LA CANASTA");
		} else {

			System.out.println("ANDROID_PRODUCT_DETAILS: LA POSICION DEL ELEMENTO SE ENCUENTRA SOBRE LA CANASTA");
		}

	}

	public static void scrollVertical(int desde, int hasta) throws Exception {

		List<MobileElement> canasta = driver.findElements(By.id("com.grability.rappi:id/layout_content_behavior"));

		if (canasta.size() > 0) {

			int y = canasta.get(0).getLocation().getY();
			if (desde >= (y - 200)) {

				desde = (y - 200);

			}

		}

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(20, desde)).waitAction().moveTo(PointOption.point(20, hasta)).release()
				.perform();

	}

	public static void scrollStores() throws Exception {

		Dimension dimension = driver.manage().window().getSize();

		int scrollHeightStart = (int) (dimension.getHeight() * 0.7);

		int scrollHeightEnd = (int) (dimension.getHeight() * 0.1);

		TouchAction action = new TouchAction(driver);
		action.press(PointOption.point(0, scrollHeightStart)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
				.moveTo(PointOption.point(0, scrollHeightEnd)).release().perform();

	}

}
