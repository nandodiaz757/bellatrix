package Plataforma.Android.Utils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

import Plataforma.testBase;

public class ExtentManagerAndroid extends testBase {

	private static ExtentReports extent;

	public synchronized static ExtentReports getReporter() {
		if (extent == null) {
			// Set HTML reporting file location
			String workingDir = System.getProperty("user.dir");
			extent = new ExtentReports(
					workingDir + "/Android/Android_Results/Reportes/ExtentReportResultsAndroid_" + fecha + ".html",
					true);
		}
		return extent;
	}
	


}
