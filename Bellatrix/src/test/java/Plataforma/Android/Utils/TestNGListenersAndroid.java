package Plataforma.Android.Utils;

import com.relevantcodes.extentreports.LogStatus;

import Plataforma.testBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import static org.testng.Assert.assertTrue;

import java.io.File;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListenersAndroid extends testBase implements ITestListener {//Regretion


	
	ITestResult iTestResult;
	private static String getTestMethodName(ITestResult iTestResult) {
		return iTestResult.getMethod().getConstructorOrMethod().getName();
	}

	@Override
	public void onStart(ITestContext iTestContext) {
		System.out.println("Starting: " + iTestContext.getName());
		iTestContext.setAttribute("WebDriver", testBase.driver);
	}

	@Override
	public void onFinish(ITestContext iTestContext) {
		System.out.println("Finish: " + iTestContext.getName());
		// Do tier down operations for extentreports reporting!

		ExtentTestManagerAndroid.endTest();
		ExtentManagerAndroid.getReporter().flush();

	}

	@Override
	public void onTestStart(ITestResult iTestResult) {
		System.out.println("Running: " + getTestMethodName(iTestResult));
	}

	@Override
	public void onTestSuccess(ITestResult iTestResult) {
		System.out.println("The test: " + getTestMethodName(iTestResult) + " -> SUCCEED");
		// ExtentReports log operation for passed tests.
		ExtentTestManagerAndroid.getTest().log(LogStatus.PASS, "Test passed");
		ExtentTestManagerAndroid.endTest();
		ExtentManagerAndroid.getReporter().flush();
	}

	@Override
	public void onTestFailure(ITestResult iTestResult) {

		Object testClass = iTestResult.getInstance();
		AndroidDriver webDriver = ((testBase) testClass).getDriver();

		// ExtentReports log and screenshot operations for failed tests.
		// Take base64Screenshot screenshot.
		String base64Screenshot = "data:image/png;base64, string";
			//	+ ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BASE64);
		System.out.println("The test: " + getTestMethodName(iTestResult) + " -> FAILED");
		
		  ExtentTestManagerAndroid.getTest().log(LogStatus.FAIL, "Test Failed " +
		  iTestResult.getThrowable().toString(),
		  ExtentTestManagerAndroid.getTest().addBase64ScreenShot(base64Screenshot));
		 
		ExtentTestManagerAndroid.endTest();
		ExtentManagerAndroid.getReporter().flush();
		
		

	}

	@Override
	public void onTestSkipped(ITestResult iTestResult) {
		System.out.println("The test: " + getTestMethodName(iTestResult) + " -> SKIPPED");
		// ExtentReports log operation for skipped tests.

		// Get driver from BaseTest and assign to local webDriver variable.
		Object testClass = iTestResult.getInstance();
		AndroidDriver webDriver = ((testBase) testClass).getDriver();

		// Take base64Screenshot screenshot.
		String base64Screenshot = "data:image/png;base64,"
				+ ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BASE64);

		ExtentTestManagerAndroid.getTest().log(LogStatus.SKIP, "Test Skipped: " + iTestResult.getThrowable().toString(),
				ExtentTestManagerAndroid.getTest().addBase64ScreenShot(base64Screenshot));
		ExtentTestManagerAndroid.endTest();
		ExtentManagerAndroid.getReporter().flush();

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
		System.out.println("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
	}

}
