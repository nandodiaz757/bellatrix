package Plataforma.Android.Utils;

import java.util.ArrayList;
import java.util.List;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import Plataforma.testBase;

public class ExcelDataProviderAndroid {

	public Object[][] testData(String excelPath, String sheetName) {

		ExcelAndroidUtils excel = new ExcelAndroidUtils(excelPath, sheetName);

		int rowCount = excel.getRowCount();

		int colCount = excel.getColCount();

		int count = 0;

		String cellData = "";

		String ejecutar = "Si";
		List<Integer> posicion = new ArrayList();

		int total = 1;

		if (excel.getCellDataString(0, colCount - 1).equals("Ejecutar")) {

			while (count < rowCount) {

				cellData = excel.getCellDataString(count, colCount - 1);

				if (cellData.equals("Si")) {

					posicion.add(count);
					total++;

				}

				count++;
			}

			rowCount = total;

		} else {

			ejecutar = "No";

		}

		Object data[][] = new Object[rowCount - 1][colCount];

		count = 0;

		for (int i = 1; i < rowCount; i++) {

			for (int j = 0; j < colCount; j++) {

				if (ejecutar.equals("Si")) {
					cellData = excel.getCellDataString(posicion.get(count), j);

					data[i - 1][j] = cellData;

				}

				if (ejecutar.equals("No")) {
					cellData = excel.getCellDataString(i, j);
					data[i - 1][j] = cellData;

				}

			}

			count++;

		}

		System.out.println("CANTIDAD DE DATOS ALMACENADOS " + data.length);

		return data;
	}

	
	//Bellatrix 
	@DataProvider(name = "dataTest")
	public Object[][] getDataTestCancelarCobroDineroRPay(ITestContext context) {

		String projectPath = System.getProperty("user.dir");
		Object data[][] = testData(projectPath + "/Android/Colombia_Data/Android_Colombia_Excel/SENDMONEYRPAY " //SENDMONEYRPAY
				+ context.getCurrentXmlTest().getParameter("pais").toString() + ".xlsx", "Envio");//Envio
		return data;
	}
	
//	@DataProvider(name = "dataTestCancelarCobroDineroRPay")
//	public Object[][] getDataTestCancelarCobroDineroRPay(ITestContext context) {
//
//		String projectPath = System.getProperty("user.dir");
//		Object data[][] = testData(projectPath + "/Android/Colombia_Data/Android_Colombia_Excel/SENDMONEYRPAY " //SENDMONEYRPAY
//				+ context.getCurrentXmlTest().getParameter("pais").toString() + ".xlsx", "Envio");//Envio
//		return data;
//	}
	

}
