package Plataforma.Android.Utils;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import Plataforma.testBase;

public class ScreenShot extends testBase {

	public static String generarEvidencia(String nombreImagen) throws Exception {

		File directory = new File("./" + fecha);

		String evidencia = "";

		directory.mkdir();

		Date fecha = new Date();

		File imagen;

		String[] nombre = null;
		if (permisosGenerarEvidencia == true) {

			try {
				if (directory.isDirectory()) {

					imagen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

					nombre = nombreImagen.split("&&");

					evidencia = directory.getAbsolutePath() + "//" + nombre[0].toString() + "_" + fecha + ".png";
					FileUtils.copyFile(imagen, new File(evidencia));

					imagen.mkdir();

				} else {

					throw new IOException("ERROR : La ruta especificada no es un directorio!");
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		if (testBase.permisosEnviarNotificacionesCorreo == true && evidencia.contains("Error")) {
		}

		return (evidencia);

	}

}
