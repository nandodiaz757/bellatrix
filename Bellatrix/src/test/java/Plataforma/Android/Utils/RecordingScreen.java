package Plataforma.Android.Utils;

import java.io.File;
import java.util.Base64;
import org.apache.commons.io.FileUtils;
import org.testng.ITestResult;

import Plataforma.testBase;
import io.appium.java_client.android.AndroidStartScreenRecordingOptions;
import io.appium.java_client.screenrecording.ScreenRecordingUploadOptions;

public class RecordingScreen extends testBase {

	public static void start(String nombre) throws Exception {

		if (permisoGenerarVideos == true) {

			driver.startRecordingScreen(new AndroidStartScreenRecordingOptions().enableForcedRestart()
					.withVideoSize("720x1280").withBitRate(50000000));
		}

	}

	public static void stopFail(String nombre) throws Exception {

		if (permisoGenerarVideos == true) {

			String video = driver.stopRecordingScreen();
			byte[] decode = Base64.getDecoder().decode(video);
			FileUtils.writeByteArrayToFile(
					new File("./Android/Android_Results/Videos/" + fecha + "_" + nombre + ".mp4"), decode);

		}

	}

	public static void stopSucces() throws Exception {

		if (permisoGenerarVideos == true) {

			driver.stopRecordingScreen();

		}

	}

}
