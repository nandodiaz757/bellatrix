package Pkg_Lib_methods;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import Plataforma.testBase;
import Plataforma.Android.Utils.ExtentTestManagerAndroid;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;


public class Cls_Lib_Methods extends testBase {
	

		public boolean WaitingForMobileElement(MobileElement ElementoAENcontrar, int intentos) throws Exception{
			      boolean ElementPresent = false;
				  int Intentos = 0;

					FluentWait<WebDriver> wait = new WebDriverWait(driver,2).pollingEvery(500, TimeUnit.SECONDS)
							.withTimeout(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
									
				   while (Intentos < (intentos) && ElementPresent==false  ) {
					   try {
						   ElementoAENcontrar.isDisplayed();
						   System.out.println("the locator" + ElementoAENcontrar + "is find");
						   ElementPresent =true;
						   }
					   catch (Exception e) {
					   System.out.println(ElementPresent + "  Not Present ");
					   Intentos ++;
					   System.out.print("#Tried: "+Intentos);
					   System.out.println(ElementPresent + " Not Present  ");
	      
					   }
				   }
			    	  
			    	  if (ElementPresent == true) {
			    		  
			           System.out.print("Successful Enable: ** :) **  ");
			        
			    	  }
			      
			      else
			      {
			    	  
			        System.out.print("Enable Failed: ** :( **  ");
			        ExtentTestManagerAndroid.GetEvidenceExtendTestAndroid("Elemento no Encontrado: "+ElementoAENcontrar, driver);
			        System.out.print("No ee encontro el Elemento: ** :( ** : " + ElementoAENcontrar);
			        
			        throw new Exception("the locator" + ElementoAENcontrar + "not find");
			        
			      }
			      System.out.println(" ");
				return ElementPresent;
			    }
		//***************end.. method waiting Element ***************//
		

		public void ScrollLeftTouchSize() {
			
			new TouchAction(driver)
			
		    .press(PointOption.point(527, 681))
		    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
		    .moveTo(PointOption.point(17, 686))
		    .release()
		    .perform();
		}
		


}
